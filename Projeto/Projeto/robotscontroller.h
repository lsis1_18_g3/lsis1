#ifndef ROBOTSCONTROLLER_H
#define ROBOTSCONTROLLER_H
#include "robots.h"
#include <QObject>
#include <QWidget>

class RobotsController : public QObject
{
    Q_OBJECT
public:
    explicit RobotsController(Robots *robots,QObject *parent = nullptr);

    Robot *novoRobot();
    bool eliminarRobot(Robot *robot);

signals:

public slots:
private:
    Robots *m_robots;
};

#endif // ROBOTSCONTROLLER_H
