#ifndef EQUIPAS_H
#define EQUIPAS_H

#include <QObject>
#include <QList>
#include <equipa.h>

class Equipas : public QObject
{
    Q_OBJECT

public:
    typedef QList<Equipa*> ListaEquipa;

    explicit Equipas(QObject *parent = nullptr);

    ListaEquipa listaEquipa() const;

    Equipa* novaEquipa();
	bool eliminarEquipa(Equipa *equipa);

signals:
    void equipaAdded(Equipa *equipa);
    void equipaRemoved(Equipa *equipa);

public slots:
private:
    ListaEquipa m_listaEquipa;
};

#endif // EQUIPAS_H
