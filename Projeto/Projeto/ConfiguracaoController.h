#pragma once

#include <QObject>
#include <Configuracoes.h>

class ConfiguracaoController : public QObject
{
	Q_OBJECT

public:
	ConfiguracaoController(Configuracoes *configs, QObject *parent = nullptr);

	Configuracao *novaConfiguracao();
	bool eliminarConfiguracao(Configuracao *config);

signals:

public slots:

private:
	Configuracoes * m_configs;

};
