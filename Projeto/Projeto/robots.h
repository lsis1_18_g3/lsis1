#ifndef ROBOTS_H
#define ROBOTS_H
#include "robot.h"
#include <QObject>
#include <QList>

class Robots : public QObject
{
    Q_OBJECT
public:
    typedef QList<Robot*> ListaRobot;

    explicit Robots(QObject *parent = nullptr);

    ListaRobot listaRobot() const;

    Robot* novoRobot();
    bool eliminarRobot(Robot *robot);

signals:
    void robotAdded(Robot *robot);
    void robotRemoved(Robot *robot);

public slots:

private:
    ListaRobot m_listaRobot;
};

#endif // ROBOTS_H
