#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPushButton>
#include <QString>
#include <QTimer>
#include <QtWidgets>
#include <QDebug>
#include <QThread>
#include <QPropertyAnimation>
#include <QTableWidget>
#include <QPropertyAnimation>
#include <QStateMachine>
#include <QCoreApplication>
#include <QStringList>
#include <QTextStream>



MainWindow::MainWindow(ProvasController *Pcontroller, RobotsController *Rcontroller, EquipasController *Econtroller, ConfiguracaoController *Ccontroller, QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow), m_Pcontroller(Pcontroller), m_Rcontroller(Rcontroller), m_Econtroller(Econtroller), m_Ccontroller(Ccontroller)
{
	ui->setupUi(this);
	ui->menuEliminar->setEnabled(false);
	ui->stackedWidget->setCurrentWidget(ui->Principal);
	inserirProvafromBD();
	inserirRobotfromBD();
	inserirEquipafromBD();
	inserirConfigfromBD();

	arduino = new QSerialPort(this);
	serialBuffer = "";
	parsed_data = "";
	estado_valor = 0;

	bool arduino_is_available = false;
	QString arduino_uno_port_name;
	
	foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()) {
		if (serialPortInfo.hasProductIdentifier() && serialPortInfo.hasVendorIdentifier()) {
			if ((serialPortInfo.productIdentifier() == arduino_uno_product_id)
				&& (serialPortInfo.vendorIdentifier() == arduino_uno_vendor_id)) {
				arduino_is_available = true; 
				arduino_uno_port_name = serialPortInfo.portName();
			}
		}

		if (arduino_is_available) {
			qDebug() << "Found the arduino port...\n";
			arduino->setPortName(arduino_uno_port_name);
			arduino->open(QSerialPort::ReadOnly); //Write 
			arduino->setBaudRate(QSerialPort::Baud9600);
			arduino->setDataBits(QSerialPort::Data8);
			arduino->setFlowControl(QSerialPort::NoFlowControl);
			arduino->setParity(QSerialPort::NoParity);
			arduino->setStopBits(QSerialPort::OneStop);
			QObject::connect(arduino, SIGNAL(readyRead()), this, SLOT(readSerial()));
		}
		else {
			qDebug() << "Couldn't find the correct port for the arduino.\n";
			QMessageBox::information(this, "Serial Port Error", "Couldn't open serial port to arduino.");
		}
	}
}

MainWindow::~MainWindow()
{
	if (arduino->isOpen()) {
		arduino->close(); 
	}
	delete ui;
}

//Prova
void MainWindow::on_actionInsProva_triggered()
{
	ui->menuEliminar->setEnabled(true);
	ui->menuModificar->setEnabled(true);
	ui->actionEliEquipa->setEnabled(false);
	ui->actionModEquipa->setEnabled(false);
	ui->actionEliRobot->setEnabled(false);
	ui->actionModRobot->setEnabled(false);
	ui->actionEliProva->setEnabled(true);
	ui->actionModProva->setEnabled(true);
	ui->actionEliConfig->setEnabled(false);
	ui->actionModConfig->setEnabled(false);
	inserirProva();
}

void MainWindow::on_actionModProva_triggered()
{
	ui->stackedWidget->setCurrentWidget(ui->pageList);
	modificarProva();
}

void MainWindow::on_actionEliProva_triggered()
{
	eliminarProva();
}

void MainWindow::inserirProva() {
	auto prova = m_Pcontroller->novaProva();
	if (prova) {
		ui->listWidget->addItem(prova->nomeProva());
		auto listItem = ui->listWidget->item(ui->listWidget->count() - 1);
		m_PentryMap.insert(listItem, prova);
		QString data = prova->DataProva().toString("yyyy.MM.dd");
		bd.inserirProvaBD(prova->nomeProva().toStdString(), prova->LocalProva().toStdString(), data.toStdString());
		inserirProvaTabela(prova);
	}
	ui->stackedWidget->setCurrentWidget(ui->pageList);
}

void MainWindow::inserirProvafromBD(){
	vector<string> nomes;
	vector<string> locais;
	vector<string> datas;

	bd.inserirProvaLista(&nomes,&locais,&datas);

	for (int i = 0; i < nomes.size(); i++) {
		
		QString nomeP = QString::fromStdString(nomes[i]);
		QString localP = QString::fromStdString(locais[i]);
		QString dataP = QString::fromStdString(datas[i]);
		QDate datep = datep.fromString(dataP, "dd  MMM  yyyy");

		auto prova = m_Pcontroller->novaProva();
		if (prova) {
			prova->setNomeProva(nomeP);
			prova->setLocalProva(localP);
			prova->setDataProva(datep);
			ui->listWidget->addItem(prova->nomeProva());
			auto listItem = ui->listWidget->item(ui->listWidget->count() - 1);
			m_PentryMap.insert(listItem, prova);
		}
		inserirProvaTabela(prova);
	}
}

void MainWindow::inserirProvaTabela(Prova *prova) {

	int row = ui->tableProva->rowCount();
	ui->tableProva->setRowCount(row+1);

	ui->tableProva->setItem(row, 0, new QTableWidgetItem(prova->nomeProva()));
	ui->tableProva->setItem(row, 1, new QTableWidgetItem(prova->LocalProva()));
	QString data = prova->DataProva().toString("dd  MMM  yyyy");
	ui->tableProva->setItem(row, 2, new QTableWidgetItem(data));
}

void MainWindow::modificarProva()
{
	auto listItem = ui->listWidget->currentItem();
	if (listItem) {
		auto prova = m_PentryMap.value(listItem);
		if (prova) {
			ui->stackedWidget->setCurrentWidget(ui->pageInserirProva);
			ui->menuMenu->setEnabled(false);
			clearProva();
			ui->nomeEdit->setPlaceholderText(prova->nomeProva());
			ui->localEdit->setPlaceholderText(prova->LocalProva());
			ui->dateEdit->setDate(prova->DataProva());
		}
	}
	
}

void MainWindow::eliminarProva() // Exemplo eliminar prova
{
	auto listItem = ui->listWidget->currentItem();
	if (listItem) {
		auto prova = m_PentryMap.value(listItem);
		if (prova) {
			bd.eliminarProvaBD(prova->nomeProva().toStdString(), prova->LocalProva().toStdString());
			eliminarRowTableProva(prova);
			if (m_Pcontroller->eliminarProva(prova)) {
				m_PentryMap.remove(listItem);
				delete listItem;
			}
		}
	}
}

void MainWindow::eliminarRowTableProva(Prova *prova) {

	QList<QTableWidgetItem *> items = ui->tableProva->findItems(prova->nomeProva(), Qt::MatchExactly);
	for (QTableWidgetItem *item : items) {
		int row = item->row();
		ui->tableProva->removeRow(row);
	}
}

void MainWindow::actualizarRowTableProva(QString nomeProvaantigo, Prova *prova) {

	QList<QTableWidgetItem *> items = ui->tableProva->findItems(nomeProvaantigo, Qt::MatchExactly);
	for (QTableWidgetItem *item : items) {
		int row = item->row();
		ui->tableProva->setItem(row, 0, new QTableWidgetItem(prova->nomeProva()));
		ui->tableProva->setItem(row, 1, new QTableWidgetItem(prova->LocalProva()));
		QString data = prova->DataProva().toString("dd  MMM  yyyy");
		ui->tableProva->setItem(row, 2, new QTableWidgetItem(data));
	}
}

void MainWindow::clearProva() {
	ui->nomeEdit->clear();
	ui->localEdit->clear();
}

void MainWindow::save()
{
	QString nomeantigo;
	auto listItem = ui->listWidget->currentItem();
	if (listItem) {
		auto prova = m_PentryMap.value(listItem);
		if (prova) {
			nomeantigo = prova->nomeProva();
			int id = bd.getIDProva(prova->nomeProva().toStdString(), prova->LocalProva().toStdString());
			if (ui->nomeEdit->text().isEmpty()) {
				prova->setNomeProva(prova->nomeProva());
			}
			else {
				prova->setNomeProva(ui->nomeEdit->text());
			}

			if (ui->localEdit->text().isEmpty()) {
				prova->setLocalProva(prova->LocalProva());
			}
			else {
				prova->setLocalProva(ui->localEdit->text());
			}

			prova->setDataProva(ui->dateEdit->date());
			QString data = prova->DataProva().toString("dd  MMM  yyyy");

			bd.modificarProvaBD(id, prova->nomeProva().toStdString(), prova->LocalProva().toStdString(), data.toStdString());
			actualizarRowTableProva(nomeantigo, prova);
			listItem->setText(prova->nomeProva());
			discard();
		}
	}
}

void MainWindow::discard()
{
	ui->stackedWidget->setCurrentWidget(ui->pageList);
	ui->menuMenu->setEnabled(true);
}

void MainWindow::reset()
{
	auto listItem = ui->listWidget->currentItem();
	if (listItem) {
		auto prova = m_PentryMap.value(listItem);
		if (prova) {
			clearProva();
			ui->menuMenu->setEnabled(false);
			ui->nomeEdit->setPlaceholderText(prova->nomeProva());
			ui->localEdit->setPlaceholderText(prova->LocalProva());
			ui->dateEdit->setDate(prova->DataProva());
		}
	}
}

void MainWindow::on_buttonBox_accepted()
{
	save();
}

void MainWindow::on_buttonBox_rejected()
{
	discard();
}

void MainWindow::on_buttonBox_clicked(QAbstractButton *button)
{
	if (button == ui->buttonBox->button(QDialogButtonBox::Reset)) {
		reset();
	}
}

void MainWindow::on_actionListarProva_triggered()
{
	ui->stackedWidget->setCurrentWidget(ui->pageList);
	ui->menuEliminar->setEnabled(true);
	ui->menuModificar->setEnabled(true);
	ui->actionEliEquipa->setEnabled(false);
	ui->actionModEquipa->setEnabled(false);
	ui->actionEliRobot->setEnabled(false);
	ui->actionModRobot->setEnabled(false);
	ui->actionEliProva->setEnabled(true);
	ui->actionModProva->setEnabled(true);
	ui->actionEliConfig->setEnabled(false);
	ui->actionModConfig->setEnabled(false);
}

void MainWindow::on_actionDetProva_triggered()
{
	ui->menuEliminar->setEnabled(false);
	ui->menuModificar->setEnabled(false);
	ui->stackedWidget->setCurrentWidget(ui->pageTableProva);
}


//Robot

void MainWindow::inserirRobot() {
	auto robot = m_Rcontroller->novoRobot();
	if (robot) {
		ui->listRobots->addItem(robot->nomeRobot());
		auto listItem = ui->listRobots->item(ui->listRobots->count() - 1);
		m_RentryMap.insert(listItem, robot);
		//bd.inserirRobotBD(robot->nomeRobot().toStdString(), robot->nomeEquipa().toStdString());
		inserirRobotTabela(robot);
		ui->stackedWidget->setCurrentWidget(ui->pageListRobots);
	}
}

void MainWindow::inserirRobotfromBD() {
	vector<string> nomesR;
	vector<string> nomesE;
	vector<string> nomesC;

	bd.inserirRobotLista(&nomesR,&nomesE, &nomesC);

	for (int i = 0; i < nomesR.size(); i++) {

		QString nomeR = QString::fromStdString(nomesR[i]);
		QString nomeE = QString::fromStdString(nomesE[i]);
		QString nomeC = QString::fromStdString(nomesC[i]);

		auto robot = m_Rcontroller->novoRobot();
		if (robot) {
			robot->setNomeRobot(nomeR);
			robot->setNomeEquipa(nomeE);
			robot->setNomeConfig(nomeC);
			ui->listRobots->addItem(robot->nomeRobot());
			auto listItem = ui->listRobots->item(ui->listRobots->count() - 1);
			m_RentryMap.insert(listItem, robot);
		}
		inserirRobotTabela(robot);
	}
}

void MainWindow::inserirRobotTabela(Robot *robot) {

	int row = ui->tableRobot->rowCount();
	ui->tableRobot->setRowCount(row + 1);

	ui->tableRobot->setItem(row, 0, new QTableWidgetItem(robot->nomeRobot()));
	ui->tableRobot->setItem(row, 1, new QTableWidgetItem(robot->nomeEquipa()));
	ui->tableRobot->setItem(row, 2, new QTableWidgetItem(robot->nomeConfig()));

}

void MainWindow::eliminarRowTableRobot(Robot *robot) {
	
	QList<QTableWidgetItem *> items = ui->tableRobot->findItems(robot->nomeRobot(), Qt::MatchExactly);
	for (QTableWidgetItem *item : items) {
		int row = item->row();
		ui->tableRobot->removeRow(row);
	}

}

void MainWindow::actulizarRowTableRobot(QString nomeAntigoRobot, Robot *robot) {

	QList<QTableWidgetItem *> items = ui->tableRobot->findItems(nomeAntigoRobot, Qt::MatchExactly);
	for (QTableWidgetItem *item : items) {
		int row = item->row();
		ui->tableRobot->setItem(row, 0, new QTableWidgetItem(robot->nomeRobot()));
		ui->tableRobot->setItem(row, 1, new QTableWidgetItem(robot->nomeEquipa()));
		ui->tableRobot->setItem(row, 2, new QTableWidgetItem(robot->nomeConfig()));
	}
}

void MainWindow::modificarRobot() {
	auto listItem = ui->listRobots->currentItem();
	if (listItem) {
		auto robot = m_RentryMap.value(listItem);
		if (robot) {
			ui->stackedWidget->setCurrentWidget(ui->pageInserirRobot);
			ui->menuMenu->setEnabled(false);
			clearRobot();
			ui->nomeRobotEdit->setPlaceholderText(robot->nomeRobot());
			int index = ui->comboBoxEquipa->findText(robot->nomeEquipa());
			if (index == -1) {
				ui->comboBoxEquipa->addItem("Sem Equipa");
			}
			else {
				ui->comboBoxEquipa->setCurrentIndex(index);
			}

			int index1 = ui->comboBoxConfig->findText(robot->nomeConfig());
			if (index1 == -1) {
				ui->comboBoxConfig->addItem("Sem Configuracao");
			}
			else {
				ui->comboBoxConfig->setCurrentIndex(index1);
			}
			

		}
	}
}

void MainWindow::eliminarRobot() {
	auto listItem = ui->listRobots->currentItem();
	if (listItem) {
		auto robot = m_RentryMap.value(listItem);
		if (robot) {
			bd.eliminarRobotBD(robot->nomeRobot().toStdString(), robot->nomeEquipa().toStdString(), robot->nomeConfig().toStdString());
			eliminarRowTableRobot(robot);
			if (m_Rcontroller->eliminarRobot(robot)) {
				m_RentryMap.remove(listItem);
				delete listItem;
			}
		}
	}
}

void MainWindow::clearRobot() {
	ui->nomeRobotEdit->clear();
}

void MainWindow::saveR() {
	QString nomeantigo;
	auto listItem = ui->listRobots->currentItem();
	if (listItem) {
		auto robot = m_RentryMap.value(listItem);
		if (robot) {
			nomeantigo = robot->nomeRobot();
			int id = bd.getIDRobot(robot->nomeRobot().toStdString(), robot->nomeEquipa().toStdString(), robot->nomeConfig().toStdString());
			if (ui->nomeRobotEdit->text().isEmpty()) {
				robot->setNomeRobot(robot->nomeRobot());
			}
			else {
				robot->setNomeRobot(ui->nomeRobotEdit->text());
			}
			bd.modificarRobotBD(id, robot->nomeRobot().toStdString(), robot->nomeEquipa().toStdString(), robot->nomeConfig().toStdString());
			actulizarRowTableRobot(nomeantigo, robot);
			listItem->setText(robot->nomeRobot());
			discardR();
		}

	}
}

void MainWindow::discardR() {
	ui->stackedWidget->setCurrentWidget(ui->pageListRobots);
	ui->menuMenu->setEnabled(true);
}

void MainWindow::resetR() {
	auto listItem = ui->listRobots->currentItem();
	if (listItem) {
		auto robot = m_RentryMap.value(listItem);
		if (robot) {
			clearRobot();
			ui->menuMenu->setEnabled(false);
			ui->nomeRobotEdit->setPlaceholderText(robot->nomeRobot());
		}

	}
}

void MainWindow::on_actionInsRobot_triggered()
{

	ui->menuEliminar->setEnabled(true);
	ui->menuModificar->setEnabled(true);
	ui->actionEliEquipa->setEnabled(false);
	ui->actionModEquipa->setEnabled(false);
	ui->actionEliRobot->setEnabled(true);
	ui->actionModRobot->setEnabled(true);
	ui->actionModProva->setEnabled(false);
	ui->actionEliProva->setEnabled(false);
	ui->actionEliConfig->setEnabled(false);
	ui->actionModConfig->setEnabled(false);
	inserirRobot();
}

void MainWindow::on_actionModRobot_triggered()
{
	modificarRobot();
}

void MainWindow::on_actionEliRobot_triggered()
{
	eliminarRobot();
}

void MainWindow::on_buttonBoxRobot_accepted()
{
	saveR();
}

void MainWindow::on_buttonBoxRobot_rejected()
{
	discardR();
}

void MainWindow::on_buttonBoxRobot_clicked(QAbstractButton *button)
{
	if (button == ui->buttonBoxRobot->button(QDialogButtonBox::Reset)) {
		resetR();
	}
}

void MainWindow::on_actionListarRobot_triggered()
{
	ui->stackedWidget->setCurrentWidget(ui->pageListRobots);
	ui->menuEliminar->setEnabled(true);
	ui->menuModificar->setEnabled(true);
	ui->actionEliEquipa->setEnabled(false);
	ui->actionModEquipa->setEnabled(false);
	ui->actionEliRobot->setEnabled(true);
	ui->actionModRobot->setEnabled(true);
	ui->actionModProva->setEnabled(false);
	ui->actionEliProva->setEnabled(false);
	ui->actionEliConfig->setEnabled(false);
	ui->actionModConfig->setEnabled(false);
}

void MainWindow::on_actionDetRobot_triggered()
{
	ui->menuEliminar->setEnabled(false);
	ui->menuModificar->setEnabled(false);

	ui->stackedWidget->setCurrentWidget(ui->pageTableRobot);
}


//Equipa

void MainWindow::inserirEquipa() {

	auto equipa = m_Econtroller->novaEquipa();
	if (equipa) {
		ui->listEquipa->addItem(equipa->nomeEquipa());
		ui->comboBoxEquipa->addItem(equipa->nomeEquipa());
		auto listItem = ui->listEquipa->item(ui->listEquipa->count() - 1);
		m_EentryMap.insert(listItem, equipa);
		bd.inserirEquipaBD(equipa->nomeEquipa().toStdString(), equipa->nomeEle1().toStdString(), equipa->nomeEle2().toStdString(), equipa->nomeEle3().toStdString(), equipa->nomeEle4().toStdString(), equipa->nomeEle5().toStdString(), equipa->nomeEle6().toStdString(), equipa->nomeEle7().toStdString(), equipa->nomeEle8().toStdString());
		inserirEquipaTabela(equipa);
		ui->stackedWidget->setCurrentWidget(ui->pageListEquipa);
	}

}

void MainWindow::inserirEquipafromBD() {

	vector<string> nome, nomec, nome2, nome3, nome4, nome5, nome6, nome7, nome8;
	QString snome, snomec, snome2, snome3, snome4, snome5, snome6, snome7, snome8;

	bd.inserirEquipaLista(&nome, &nomec, &nome2, &nome3, &nome4, &nome5, &nome6, &nome7, &nome8);

	for (int i = 0; i < nome.size(); i++) {

		snome = QString::fromStdString(nome[i]);
		snomec = QString::fromStdString(nomec[i]);
		snome2 = QString::fromStdString(nome2[i]);
		snome3 = QString::fromStdString(nome3[i]);
		snome4 = QString::fromStdString(nome4[i]);
		snome5 = QString::fromStdString(nome5[i]);
		snome6 = QString::fromStdString(nome6[i]);
		snome7 = QString::fromStdString(nome7[i]);
		snome8 = QString::fromStdString(nome8[i]);

		auto equipa = m_Econtroller->novaEquipa();
		if (equipa) {

			equipa->setNomeEquipa(snome);
			equipa->setNomeEle1(snomec);
			equipa->setNomeEle2(snome2);
			equipa->setNomeEle3(snome3);
			equipa->setNomeEle4(snome4);
			equipa->setNomeEle5(snome5);
			equipa->setNomeEle6(snome6);
			equipa->setNomeEle7(snome7);
			equipa->setNomeEle8(snome8);

			ui->listEquipa->addItem(equipa->nomeEquipa());
			ui->comboBoxEquipa->addItem(equipa->nomeEquipa());
			auto listItem = ui->listEquipa->item(ui->listEquipa->count() - 1);
			m_EentryMap.insert(listItem, equipa);
		}
		inserirEquipaTabela(equipa);
	}
}

void MainWindow::inserirEquipaTabela(Equipa *equipa) {

	int row = ui->tableEquipa->rowCount();
	ui->tableEquipa->setRowCount(row + 1);

	ui->tableEquipa->setItem(row, 0, new QTableWidgetItem(equipa->nomeEquipa()));
	ui->tableEquipa->setItem(row, 1, new QTableWidgetItem(equipa->nomeEle1()));
	ui->tableEquipa->setItem(row, 2, new QTableWidgetItem(equipa->nomeEle2()));
	ui->tableEquipa->setItem(row, 3, new QTableWidgetItem(equipa->nomeEle3()));
	ui->tableEquipa->setItem(row, 4, new QTableWidgetItem(equipa->nomeEle4()));
	ui->tableEquipa->setItem(row, 5, new QTableWidgetItem(equipa->nomeEle5()));
	ui->tableEquipa->setItem(row, 6, new QTableWidgetItem(equipa->nomeEle6()));
	ui->tableEquipa->setItem(row, 7, new QTableWidgetItem(equipa->nomeEle7()));
	ui->tableEquipa->setItem(row, 8, new QTableWidgetItem(equipa->nomeEle8()));

}

void MainWindow::actualizarRowTableEquipa(QString nomeAntigoEquipa, Equipa *equipa) {

	QList<QTableWidgetItem *> items = ui->tableEquipa->findItems(nomeAntigoEquipa, Qt::MatchExactly);
	for (QTableWidgetItem *item : items) {
		int row = item->row();

		ui->tableEquipa->setItem(row, 0, new QTableWidgetItem(equipa->nomeEquipa()));
		ui->tableEquipa->setItem(row, 1, new QTableWidgetItem(equipa->nomeEle1()));
		ui->tableEquipa->setItem(row, 2, new QTableWidgetItem(equipa->nomeEle2()));
		ui->tableEquipa->setItem(row, 3, new QTableWidgetItem(equipa->nomeEle3()));
		ui->tableEquipa->setItem(row, 4, new QTableWidgetItem(equipa->nomeEle4()));
		ui->tableEquipa->setItem(row, 5, new QTableWidgetItem(equipa->nomeEle5()));
		ui->tableEquipa->setItem(row, 6, new QTableWidgetItem(equipa->nomeEle6()));
		ui->tableEquipa->setItem(row, 7, new QTableWidgetItem(equipa->nomeEle7()));
		ui->tableEquipa->setItem(row, 8, new QTableWidgetItem(equipa->nomeEle8()));
	}
}

void MainWindow::eliminarRowTableEquipa(Equipa *equipa) {

	QList<QTableWidgetItem *> items = ui->tableEquipa->findItems(equipa->nomeEquipa(), Qt::MatchExactly);
	for (QTableWidgetItem *item : items) {
		int row = item->row();
		ui->tableEquipa->removeRow(row);
	}

}

void MainWindow::modificarEquipa() {
	auto listItem = ui->listEquipa->currentItem();
	if (listItem) {
		auto equipa = m_EentryMap.value(listItem);
		if (equipa) {
			ui->stackedWidget->setCurrentWidget(ui->pageInserirEquipa);
			ui->menuMenu->setEnabled(false);
			clearEquipa();
			ui->nomeEquipaEdit->setPlaceholderText(equipa->nomeEquipa());
			ui->nomeEquipaEdit->setPlaceholderText(equipa->nomeEquipa());
			ui->nomeEle1Edit->setPlaceholderText(equipa->nomeEle1());
			ui->nomeEle2Edit->setPlaceholderText(equipa->nomeEle2());
			ui->nomeEle3Edit->setPlaceholderText(equipa->nomeEle3());
			ui->nomeEle4Edit->setPlaceholderText(equipa->nomeEle4());
			ui->nomeEle5Edit->setPlaceholderText(equipa->nomeEle5());
			ui->nomeEle6Edit->setPlaceholderText(equipa->nomeEle6());
			ui->nomeEle7Edit->setPlaceholderText(equipa->nomeEle7());
			ui->nomeEle8Edit->setPlaceholderText(equipa->nomeEle8());
		}
	}
}

void MainWindow::eliminarEquipa() {

	auto listItem = ui->listEquipa->currentItem();
	if (listItem) {
		auto equipa = m_EentryMap.value(listItem);
		if (equipa) {
			bd.eliminarEquipaBD(equipa->nomeEquipa().toStdString());
			eliminarRowTableEquipa(equipa);
			if (m_Econtroller->eliminarEquipa(equipa)) {
				m_PentryMap.remove(listItem);
				delete listItem;
			}
		}
	}
}

void MainWindow::saveE() {
	QString nomeantigo;
	auto listItem = ui->listEquipa->currentItem();
	if (listItem) {
		auto equipa = m_EentryMap.value(listItem);
		if (equipa) {
			nomeantigo = equipa->nomeEquipa();
			int id = bd.getIDEquipa(equipa->nomeEquipa().toStdString());
			int index = ui->comboBoxEquipa->findText(equipa->nomeEquipa());

			if (ui->nomeEquipaEdit->text().isEmpty()) {
				equipa->setNomeEquipa(equipa->nomeEquipa());
			}
			else {
				equipa->setNomeEquipa(ui->nomeEquipaEdit->text());
			}

			if (ui->nomeEle1Edit->text().isEmpty()) {
				equipa->setNomeEle1(equipa->nomeEle1());

			}
			else {
				equipa->setNomeEle1(ui->nomeEle2Edit->text());
			}

			if (ui->nomeEle2Edit->text().isEmpty()) {
				equipa->setNomeEle2(equipa->nomeEle2());

			}
			else {
				equipa->setNomeEle2(ui->nomeEle2Edit->text());
			}

			if (ui->nomeEle3Edit->text().isEmpty()) {
				equipa->setNomeEle3(equipa->nomeEle3());

			}
			else {
				equipa->setNomeEle3(ui->nomeEle3Edit->text());
			}

			if (ui->nomeEle4Edit->text().isEmpty()) {
				equipa->setNomeEle4(equipa->nomeEle4());

			}
			else {
				equipa->setNomeEle4(ui->nomeEle3Edit->text());
			}

			if (ui->nomeEle5Edit->text().isEmpty()) {
				equipa->setNomeEle5(equipa->nomeEle5());

			}
			else {
				equipa->setNomeEle5(ui->nomeEle5Edit->text());
			}

			if (ui->nomeEle6Edit->text().isEmpty()) {
				equipa->setNomeEle6(equipa->nomeEle6());

			}
			else {
				equipa->setNomeEle6(ui->nomeEle6Edit->text());
			}

			if (ui->nomeEle7Edit->text().isEmpty()) {
				equipa->setNomeEle7(equipa->nomeEle7());

			}
			else {
				equipa->setNomeEle7(ui->nomeEle7Edit->text());
			}

			if (ui->nomeEle8Edit->text().isEmpty()) {
				equipa->setNomeEle8(equipa->nomeEle8());

			}
			else {
				equipa->setNomeEle8(ui->nomeEle8Edit->text());
			}

			listItem->setText(equipa->nomeEquipa());
			bd.modificarEquipaBD(id, equipa->nomeEquipa().toStdString(), equipa->nomeEle1().toStdString(), equipa->nomeEle2().toStdString(), equipa->nomeEle3().toStdString(), equipa->nomeEle4().toStdString(), equipa->nomeEle5().toStdString(), equipa->nomeEle6().toStdString(), equipa->nomeEle7().toStdString(), equipa->nomeEle8().toStdString());
			actualizarRowTableEquipa(nomeantigo, equipa);
			ui->comboBoxEquipa->setItemText(index, equipa->nomeEquipa());
			discardE();
		}

	}
}

void MainWindow::discardE() {
	ui->stackedWidget->setCurrentWidget(ui->pageListEquipa);
	ui->menuMenu->setEnabled(true);
}

void MainWindow::resetE() {
	auto listItem = ui->listEquipa->currentItem();
	if (listItem) {
		auto equipa = m_EentryMap.value(listItem);
		if (equipa) {
			ui->menuMenu->setEnabled(false);
			clearEquipa();
			ui->nomeEquipaEdit->setPlaceholderText(equipa->nomeEquipa());
			ui->nomeEle1Edit->setPlaceholderText(equipa->nomeEle1());
			ui->nomeEle2Edit->setPlaceholderText(equipa->nomeEle2());
			ui->nomeEle3Edit->setPlaceholderText(equipa->nomeEle3());
			ui->nomeEle4Edit->setPlaceholderText(equipa->nomeEle4());
			ui->nomeEle5Edit->setPlaceholderText(equipa->nomeEle5());
			ui->nomeEle6Edit->setPlaceholderText(equipa->nomeEle6());
			ui->nomeEle7Edit->setPlaceholderText(equipa->nomeEle7());
			ui->nomeEle8Edit->setPlaceholderText(equipa->nomeEle8());
		}
	}
}

void MainWindow::clearEquipa() {
	ui->nomeEquipaEdit->clear();
	ui->nomeEle1Edit->clear();
	ui->nomeEle2Edit->clear();
	ui->nomeEle3Edit->clear();
	ui->nomeEle4Edit->clear();
	ui->nomeEle5Edit->clear();
	ui->nomeEle6Edit->clear();
	ui->nomeEle7Edit->clear();
	ui->nomeEle8Edit->clear();
}

void MainWindow::on_actionListarEquipa_triggered()
{
	ui->stackedWidget->setCurrentWidget(ui->pageListEquipa);
	ui->menuEliminar->setEnabled(true);
	ui->menuModificar->setEnabled(true);
	ui->actionEliEquipa->setEnabled(true);
	ui->actionModEquipa->setEnabled(true);
	ui->actionEliRobot->setEnabled(false);
	ui->actionModRobot->setEnabled(false);
	ui->actionModProva->setEnabled(false);
	ui->actionEliProva->setEnabled(false);
	ui->actionEliConfig->setEnabled(false);
	ui->actionModConfig->setEnabled(false);
}

void MainWindow::on_actionInsEquipa_triggered()
{
	ui->menuEliminar->setEnabled(true);
	ui->menuModificar->setEnabled(true);
	ui->actionEliEquipa->setEnabled(true);
	ui->actionModEquipa->setEnabled(true);
	ui->actionEliRobot->setEnabled(false);
	ui->actionModRobot->setEnabled(false);
	ui->actionModProva->setEnabled(false);
	ui->actionEliProva->setEnabled(false);
	ui->actionEliConfig->setEnabled(false);
	ui->actionModConfig->setEnabled(false);
	inserirEquipa();
}

void MainWindow::on_actionModEquipa_triggered()
{
	ui->stackedWidget->setCurrentWidget(ui->pageListEquipa);
	modificarEquipa();
}

void MainWindow::on_actionEliEquipa_triggered()
{
	eliminarEquipa();
}

void MainWindow::on_buttonBoxEquipa_accepted()
{
	saveE();
}

void MainWindow::on_buttonBoxEquipa_rejected()
{
	discardE();
}

void MainWindow::on_buttonBoxEquipa_clicked(QAbstractButton *button)
{
	if (button == ui->buttonBoxEquipa->button(QDialogButtonBox::Reset)) {
		resetE();
	}
}

void MainWindow::on_actionDetEquipa_triggered()
{
	ui->menuEliminar->setEnabled(false);
	ui->menuModificar->setEnabled(false);
	ui->stackedWidget->setCurrentWidget(ui->pageTableEquipa);
}


//Configuracao

void MainWindow::inserirConfig() {

	auto config = m_Ccontroller->novaConfiguracao();
	if (config) {
		ui->listConfig->addItem(config->nomeConfig());
		ui->comboBoxConfig->addItem(config->nomeConfig());
		auto listItem = ui->listConfig->item(ui->listConfig->count() - 1);
		m_CentryMap.insert(listItem, config);
		bd.inserirConfigBD(config->nomeConfig().toStdString(), config->tempoViragem().toStdString(), config->nSesores().toStdString(), config->metodo().toStdString());
		inserirConfigTabela(config);
	}
	ui->stackedWidget->setCurrentWidget(ui->pageListConfig);
}

void MainWindow::inserirConfigTabela(Configuracao *config) {

	int row = ui->tableConfig->rowCount();
	ui->tableConfig->setRowCount(row + 1);

	ui->tableConfig->setItem(row, 0, new QTableWidgetItem(config->nomeConfig()));
	ui->tableConfig->setItem(row, 1, new QTableWidgetItem(config->metodo()));
	ui->tableConfig->setItem(row, 2, new QTableWidgetItem(config->nSesores()));
	ui->tableConfig->setItem(row, 3, new QTableWidgetItem(config->tempoViragem()));
}

void MainWindow::actulizarRowTableConfig(QString nomeantigo, Configuracao *config) {

	QList<QTableWidgetItem *> items = ui->tableConfig->findItems(nomeantigo, Qt::MatchExactly);
	for (QTableWidgetItem *item : items) {
		int row = item->row();
		ui->tableConfig->setItem(row, 0, new QTableWidgetItem(config->nomeConfig()));
		ui->tableConfig->setItem(row, 1, new QTableWidgetItem(config->metodo()));
		ui->tableConfig->setItem(row, 2, new QTableWidgetItem(config->nSesores()));
		ui->tableConfig->setItem(row, 3, new QTableWidgetItem(config->tempoViragem()));
	}
}

void MainWindow::inserirConfigfromBD() {
	vector<string> nomes;
	vector<string> sensores;
	vector<string> tempos;
	vector<string> metodos;

	bd.inserirConfigLista(&nomes, &sensores, &tempos, &metodos);

	for (int i = 0; i < nomes.size(); i++) {

		QString nomeC = QString::fromStdString(nomes[i]);
		QString nSensores = QString::fromStdString(sensores[i]);
		QString tempoV = QString::fromStdString(tempos[i]);
		QString metodo = QString::fromStdString(metodos[i]);

		auto config = m_Ccontroller->novaConfiguracao();
		if (config) {
			config->setNomeConfig(nomeC);
			config->setNSensores(nSensores);
			config->setTempoViragem(tempoV);
			config->setMetodo(metodo);
			ui->listConfig->addItem(config->nomeConfig());
			ui->comboBoxConfig->addItem(config->nomeConfig());
			auto listItem = ui->listConfig->item(ui->listConfig->count() - 1);
			m_CentryMap.insert(listItem, config);
		}
		inserirConfigTabela(config);
	}
}

void MainWindow::eliminarRowTableConfig(Configuracao *config) {

	QList<QTableWidgetItem *> items = ui->tableConfig->findItems(config->nomeConfig(), Qt::MatchExactly);
	for (QTableWidgetItem *item : items) {
		int row = item->row();
		ui->tableConfig->removeRow(row);
	}
}

void MainWindow::modificarConfiguracao() {

	auto listItem = ui->listConfig->currentItem();
	if (listItem) {
		auto config = m_CentryMap.value(listItem);
		if (config) {
			ui->stackedWidget->setCurrentWidget(ui->pageInsertConfig);
			ui->menuMenu->setEnabled(false);
			clearConfig();
			ui->nomeConfigEdit->setPlaceholderText(config->nomeConfig());
			ui->metodoEdit->setCurrentText(config->metodo());
			ui->nSensoresEdit->setCurrentText(config->nSesores());
			ui->tempoViragemEdit->setPlaceholderText(config->tempoViragem());
		}
	}

}

void MainWindow::eliminarConfiguracao() {

	auto listItem = ui->listConfig->currentItem();
	if (listItem) {
		auto config = m_CentryMap.value(listItem);
		if (config) {
			bd.eliminarConfigBD(config->nomeConfig().toStdString(), config->tempoViragem().toStdString(), config->nSesores().toStdString(), config->metodo().toStdString());
			eliminarRowTableConfig(config);
			if (m_Ccontroller->eliminarConfiguracao(config)) {
				m_CentryMap.remove(listItem);
				delete listItem;
			}
		}
	}

}

void MainWindow::saveC() {

	QString nomeantigo;
	auto listItem = ui->listConfig->currentItem();
	if (listItem) {
		auto config = m_CentryMap.value(listItem);
		if (config) {
			nomeantigo = config->nomeConfig();
			int index = ui->comboBoxConfig->findText(config->nomeConfig());
			int id = bd.getIDConfig(config->nomeConfig().toStdString());
			if (ui->nomeConfigEdit->text().isEmpty()) {
				config->setNomeConfig(config->nomeConfig());
			}
			else {
				config->setNomeConfig(ui->nomeConfigEdit->text());
			}

			if (ui->tempoViragemEdit->text().isEmpty()) {
				config->setTempoViragem(config->tempoViragem());
			}
			else {
				config->setTempoViragem(ui->tempoViragemEdit->text());
			}

			config->setMetodo(ui->metodoEdit->currentText());
			config->setNSensores(ui->nSensoresEdit->currentText());

			bd.modificarConfigBD(id, config->nomeConfig().toStdString(), config->tempoViragem().toStdString(), config->nSesores().toStdString(), config->metodo().toStdString());
			actulizarRowTableConfig(nomeantigo, config);
			listItem->setText(config->nomeConfig());
			ui->comboBoxEquipa->setItemText(index, config->nomeConfig());
			discardC();
		}
	}
}

void MainWindow::discardC() {
	ui->stackedWidget->setCurrentWidget(ui->pageListConfig);
	ui->menuMenu->setEnabled(true);
}

void MainWindow::resetC() {
	auto listItem = ui->listConfig->currentItem();
	if (listItem) {
		auto config = m_CentryMap.value(listItem);
		if (config) {
			clearConfig();
			ui->menuMenu->setEnabled(false);
			ui->nomeConfigEdit->setPlaceholderText(config->nomeConfig());
			ui->metodoEdit->setCurrentText(config->metodo());
			ui->nSensoresEdit->setCurrentText(config->nSesores());
			ui->tempoViragemEdit->setPlaceholderText(config->tempoViragem());
		}

	}
}

void MainWindow::clearConfig() {

	ui->nomeConfigEdit->clear();
	ui->tempoViragemEdit->clear();

}

void MainWindow::on_actionLisConfi_triggered()
{
	ui->stackedWidget->setCurrentWidget(ui->pageListConfig);
	ui->menuEliminar->setEnabled(true);
	ui->menuModificar->setEnabled(true);
	ui->actionEliEquipa->setEnabled(false);
	ui->actionModEquipa->setEnabled(false);
	ui->actionEliRobot->setEnabled(false);
	ui->actionModRobot->setEnabled(false);
	ui->actionEliProva->setEnabled(false);
	ui->actionModProva->setEnabled(false);
	ui->actionEliConfig->setEnabled(true);
	ui->actionModConfig->setEnabled(true);

}

void MainWindow::on_actionDetConfig_triggered()
{
	ui->menuEliminar->setEnabled(false);
	ui->menuModificar->setEnabled(false);
	ui->stackedWidget->setCurrentWidget(ui->pageTableConfig);
}

void MainWindow::on_actionInsConfig_triggered()
{
	ui->menuEliminar->setEnabled(true);
	ui->menuModificar->setEnabled(true);
	ui->actionEliEquipa->setEnabled(false);
	ui->actionModEquipa->setEnabled(false);
	ui->actionEliRobot->setEnabled(false);
	ui->actionModRobot->setEnabled(false);
	ui->actionEliProva->setEnabled(false);
	ui->actionModProva->setEnabled(false);
	ui->actionEliConfig->setEnabled(true);
	ui->actionModConfig->setEnabled(true);
	inserirConfig();
}

void MainWindow::on_actionModConfig_triggered()
{
	ui->stackedWidget->setCurrentWidget(ui->pageListConfig);
	modificarConfiguracao();
}

void MainWindow::on_actionEliConfig_triggered()
{
	eliminarConfiguracao();
}

void MainWindow::on_buttonBoxConfig_accepted()
{
	saveC();
}

void MainWindow::on_buttonBoxConfig_rejected()
{
	discardC();
}

void MainWindow::on_buttonBoxConfig_clicked(QAbstractButton *button)
{
	if (button == ui->buttonBoxConfig->button(QDialogButtonBox::Reset)) {
		resetC();
	}
}


//Modo Prova
void MainWindow::on_actionIniciar_Prova_triggered()
{
	ui->stackedWidget->setCurrentWidget(ui->pageProva);
}

void MainWindow::ModoProva(int fase) {

		switch (fase)
		{

		case 1: //Parado
			qDebug() << "Fase 1";
			ui->pushParado->setStyleSheet("background-color: darkgreen");

			ui->pushDireita->setStyleSheet("background-color: lightgrey");
			ui->pushLED->setStyleSheet("background-color: lightgrey");
			ui->pushEsquerda->setStyleSheet("background-color: lightgrey");
			ui->pushFrente->setStyleSheet("background-color: lightgrey");
			ui->pushVentoinha->setStyleSheet("background-color: lightgrey");

			ui->pageProva->update();
			break;
		case 2: //Frente
			qDebug() << "Fase 2";
			ui->pushFrente->setStyleSheet("background-color: darkgreen");

			ui->pushDireita->setStyleSheet("background-color: lightgrey");
			ui->pushLED->setStyleSheet("background-color: lightgrey");
			ui->pushEsquerda->setStyleSheet("background-color: lightgrey");
			ui->pushParado->setStyleSheet("background-color: lightgrey");
			ui->pushVentoinha->setStyleSheet("background-color: lightgrey");
			break;
		case 3: // Direita
			qDebug() << "Fase 3";
			ui->pushDireita->setStyleSheet("background-color: darkgreen");

			ui->pushFrente->setStyleSheet("background-color: lightgrey");
			ui->pushLED->setStyleSheet("background-color: lightgrey");
			ui->pushEsquerda->setStyleSheet("background-color: lightgrey");
			ui->pushParado->setStyleSheet("background-color: lightgrey");
			ui->pushVentoinha->setStyleSheet("background-color: lightgrey");
			break;
		case 4: //Esquerda
			qDebug() << "Fase 4";
			ui->pushEsquerda->setStyleSheet("background-color: darkgreen");

			ui->pushDireita->setStyleSheet("background-color: lightgrey");
			ui->pushFrente->setStyleSheet("background-color: lightgrey");
			ui->pushLED->setStyleSheet("background-color: lightgrey");
			ui->pushParado->setStyleSheet("background-color: lightgrey");
			ui->pushVentoinha->setStyleSheet("background-color: lightgrey");
			break;
		case 5: // LED
			qDebug() << "Fase 5";
			ui->pushLED->setStyleSheet("background-color: darkred");

			ui->pushEsquerda->setStyleSheet("background-color: lightgrey");
			ui->pushDireita->setStyleSheet("background-color: lightgrey");
			ui->pushFrente->setStyleSheet("background-color: lightgrey");
			ui->pushParado->setStyleSheet("background-color: lightgrey");
			ui->pushVentoinha->setStyleSheet("background-color: lightgrey");
			break;
		case 6: // Ventoinha
			qDebug() << "Fase 6";
			ui->pushVentoinha->setStyleSheet("background-color: darkgreen");

			ui->pushLED->setStyleSheet("background-color: lightgrey");
			ui->pushEsquerda->setStyleSheet("background-color: lightgrey");
			ui->pushDireita->setStyleSheet("background-color: lightgrey");
			ui->pushFrente->setStyleSheet("background-color: lightgrey");
			ui->pushParado->setStyleSheet("background-color: lightgrey");
			break;
		}

}

void MainWindow::on_pushButton_clicked()
{
	/*timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(Fases()));
	timer->start(2000);*/
	if (arduino->isWritable()) {
		arduino->write("1");
	}
	else {
		qDebug() << "Impossivel escrever!";
	}
}

void MainWindow::Fases() {
	
	int estado = estados[0];
	
	ModoProva(estado);

	if (estados.size() == 0) {

		QMessageBox msgBox1;
		msgBox1.setText("Prova Finalizada");
		msgBox1.exec();
		timer->stop();
	}
	else {
		estados.erase(estados.begin());
	}
}

void MainWindow::receberFases() {
	//
	//	const int argumentCount = QCoreApplication::arguments().size();
	//	const QStringList argumentList = QCoreApplication::arguments();
	//
	//	QTextStream standardOutput(stdout);
	//
	//	if (argumentCount == 1) {
	//		standardOutput << QObject::tr("Usage: %1 <COM3> [9600]").arg(argumentList.first()) << endl;
	//	}
	//
	//	QSerialPort serialPort;
	//	const QString serialPortName = argumentList.at(1);
	//	serialPort.setPortName(serialPortName);
	//
	//	const int serialPortBaudRate = (argumentCount > 2) ? argumentList.at(2).toInt() : QSerialPort::Baud9600;
	//	serialPort.setBaudRate(serialPortBaudRate);
	//
	//	if (!serialPort.open(QIODevice::ReadOnly)) {
	//		standardOutput << QObject::tr("Failed to open port %1, error: %2").arg(serialPortName).arg(serialPort.error()) << endl;
	//	}
	//
	//	QByteArray readData = serialPort.readAll();
	//	while (serialPort.waitForReadyRead(5000))
	//		readData.append(serialPort.readAll());
	//
	//	if (serialPort.error() == QSerialPort::ReadError) {
	//		standardOutput << QObject::tr("Failed to read from port %1, error: %2").arg(serialPortName).arg(serialPort.errorString()) << endl;
	//	}
	//	else if (serialPort.error() == QSerialPort::TimeoutError && readData.isEmpty()) {
	//		standardOutput << QObject::tr("No data was currently available"" for reading from port %1").arg(serialPortName) << endl;
	//
	//	}
	//
	//	standardOutput << QObject::tr("Data successfully received from port %1").arg(serialPortName) << endl;
	//	standardOutput << readData << endl;
	//
}

//Arduino

void MainWindow::readSerial()
{
	QStringList buffer_split = serialBuffer.split("\n"); 																	
														
	if (buffer_split.length() < 2) {
		serialData = arduino->readAll();
		serialBuffer = serialBuffer + QString::fromStdString(serialData.toStdString());
		serialData.clear();
	}
	else {
		serialBuffer = "";
		qDebug() << buffer_split << "\n";
		parsed_data = buffer_split[1];
		estado_valor = parsed_data.toInt(); 
		qDebug() << "Estado: " << estado_valor << "\n";
		updateEstados(estado_valor);
	}
}

void MainWindow::updateEstados(int estado)
{
	ui->stackedWidget->setCurrentWidget(ui->pageProva);
	ModoProva(estado);
}