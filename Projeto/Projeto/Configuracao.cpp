#include "Configuracao.h"

Configuracao::Configuracao(QObject *parent) : QObject(parent)
{
}

QString Configuracao::nomeConfig() const {

	return m_nomeConfig;
}

void Configuracao::setNomeConfig(const QString &nomeConfig) {

	m_nomeConfig = nomeConfig;
}

QString Configuracao:: metodo() const {

	return m_metodo;
}

void Configuracao::setMetodo(const QString &metodo) {

	m_metodo = metodo;
}

QString Configuracao::nSesores() const {

	return m_nSensores;
}

void Configuracao::setNSensores(const QString &nSensores) {

	m_nSensores = nSensores;
}

QString Configuracao::tempoViragem() const {

	return m_tempoViragem;
}

void Configuracao::setTempoViragem(const QString &tempoViragem) {

	m_tempoViragem = tempoViragem;
}
