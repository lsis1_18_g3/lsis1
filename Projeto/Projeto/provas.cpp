#include "Provas.h"

Provas::Provas(QObject *parent) :QObject(parent) {

}

Provas::ListaProva Provas::listaProva() const {
	return m_listaProva;
}

Prova *Provas::novaProva() {

	auto result = new Prova(this);
	if (result != nullptr) {
		m_listaProva.append(result);
	}
	return result;
}

bool Provas::eliminarProva(Prova *prova) {
	if (m_listaProva.contains(prova)) {
		m_listaProva.removeOne(prova);
		delete prova;
		return true;
	}
	return false;
}

