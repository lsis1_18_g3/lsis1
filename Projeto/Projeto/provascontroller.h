#ifndef PROVASCONTROLLER_H
#define PROVASCONTROLLER_H

#include "Provas.h"
#include <iostream>
#include <QObject>

class ProvasController : public QObject
{
    Q_OBJECT

public:
    explicit ProvasController(Provas *provas, QObject *parent = nullptr);

    Prova *novaProva();
    bool eliminarProva(Prova *prova);

signals:

    public slots:

private:

    Provas * m_provas;
};
#endif
