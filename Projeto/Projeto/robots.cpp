#include "robots.h"

Robots::Robots(QObject *parent) : QObject(parent)
{

}

Robots::ListaRobot Robots::listaRobot() const {
	return m_listaRobot;
}

Robot *Robots::novoRobot() {
	auto result = new Robot(this);
	if (result != nullptr) {
		m_listaRobot.append(result);
	}
	return result;
}

bool Robots::eliminarRobot(Robot *robot) {
	if (m_listaRobot.contains(robot)) {
		m_listaRobot.removeOne(robot);
		delete robot;
		return true;
	}
	return false;
}
