#include "ProvasController.h"


ProvasController::ProvasController(Provas *provas, QObject *parent) : QObject(parent), m_provas(provas) {

    Q_ASSERT(provas != nullptr);
}

Prova *ProvasController::novaProva()
{
    auto result = m_provas->novaProva();
    if (result != nullptr) {
        result->setNomeProva("Nova prova");
        result->setLocalProva("Indefinido");
    }
    return result;
}

bool ProvasController::eliminarProva(Prova *prova)
{
    return m_provas->eliminarProva(prova);
}
