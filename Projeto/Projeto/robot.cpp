#include "robot.h"

Robot::Robot(QObject *parent) : QObject(parent)
{

}

QString Robot::nomeRobot() const
{
	return m_nomeRobot;
}

void Robot::setNomeRobot(const QString &nomeRobot)
{
	m_nomeRobot = nomeRobot;
}

QString Robot::nomeEquipa() const
{
	return m_nomeEquipa;
}

void Robot::setNomeEquipa(const QString &nomeEquipa)
{
	m_nomeEquipa = nomeEquipa;
}

QString Robot::nomeConfig() const
{
	return m_nomeConfig;
}

void Robot::setNomeConfig(const QString &nomeConfig)
{
	m_nomeConfig = nomeConfig;
}
