#include "Configuracoes.h"

Configuracoes::Configuracoes(QObject *parent): QObject(parent)
{
}

Configuracoes::ListaConfig Configuracoes::listaconfi() const
{
	return m_listaconfig;
}

Configuracao *Configuracoes::novaConfiguracao()
{
	auto result = new Configuracao(this);
	if (result != nullptr) {
		m_listaconfig.append(result);
	}
	return result;
}

bool Configuracoes::eliminarConfiguracao(Configuracao * config)
{
	if (m_listaconfig.contains(config)) {
		emit configRemoved(config);
		m_listaconfig.removeOne(config);
		delete config;
		return true;
	}
	return false;
}

