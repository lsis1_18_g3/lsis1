#include "Basedados.h"
#include "mainwindow.h"


void BaseDados::Ligar() {

	driver = get_driver_instance();
	con = driver->connect("tcp://127.0.0.1:3306", "Diogo", "lsis1");
	con->setSchema("lsis1");
}
 
void BaseDados::desligar() {
		delete res;
		delete stmt;
		con->close();
		delete con;
	
}

int BaseDados::getIDProva(string nomeProva, string localProva) {
	int id;


	Ligar();

	stmt = con->createStatement();

	res = stmt->executeQuery("SELECT * FROM prova where NomeProva LIKE '" + nomeProva + "' AND LocalProva LIKE  '" + localProva + "'");

	while (res->next()) {
		id = res->getInt(1);
	}

	delete res;
	delete stmt;
	delete con;

	return id;
}

int BaseDados::getIDConfig(string nomeConfig) {
	int id;

	Ligar();

	stmt = con->createStatement();

	res = stmt->executeQuery("SELECT * FROM configuracao where NomeConfiguracao LIKE '" + nomeConfig + "';");

	while (res->next()) {
		id = res->getInt(1);
	}

	delete res;
	delete stmt;
	delete con;

	return id;
}

string BaseDados::getNomeConfig(int id) {
	string nome;

	stmt = con->createStatement();

	res = stmt->executeQuery("SELECT NomeConfiguracao FROM configuracao where IDConfiguracao = '" + to_string(id) + "';");


	while (res->next()) {
		nome = res->getString("NomeConfiguracao");
	}

	return nome;
}

string BaseDados::getNomeEquipa(int idEquipa) {
	string nome;
	sql::ResultSet *res1;
	sql::Statement *stmt1;

	stmt1 = con->createStatement();

	res1 = stmt1->executeQuery("SELECT NomeEquipa FROM equipa where IDEquipa = '" + to_string(idEquipa) + "';");


	while (res1->next()) {
		nome = res1->getString("NomeEquipa");
	}

	return nome;
	delete res1;
	delete stmt1;
}

int BaseDados::getIDRobot(string nomeRobot, string nomeEquipa, string nomeC) {
	int id, idC;


	id = getIDEquipa(nomeEquipa);
	string idEquipa = to_string(id);

	idC = getIDConfig(nomeC);
	string idConfig = to_string(idC);

	Ligar();

	stmt = con->createStatement();

	res = stmt->executeQuery("SELECT * FROM robot where NomeRobot LIKE '" + nomeRobot + "' AND IDEquipa LIKE  '" + idEquipa + "' AND IDConfiguracao LIKE '" + idConfig + "'");

	while (res->next()) {
		id = res->getInt(1);
	}


	delete res;
	delete stmt;
	delete con;

	return id;
}

int BaseDados::getIDEquipa(string nomeEquipa) {
	int idE;

	Ligar();

	stmt = con->createStatement();


	res = stmt->executeQuery("SELECT IDEquipa FROM equipa WHERE NomeEquipa = '" + nomeEquipa + "';");

	while (res->next()) {
			idE = res->getInt(1);
	}
	
	delete res;
	delete stmt;
	delete con;

	return idE;
}

void BaseDados::inserirProvaBD(string nomeProva, string localProva, string data) {

	Ligar();

	stmt = con->createStatement();


	stmt->execute("INSERT INTO prova(NomeProva,LocalProva,DataProva) Values('" + nomeProva + "','" + localProva + "','" + data + "');");

	delete stmt;
	delete con;
}

void BaseDados::inserirConfigBD(string nomeConfig, string tempoV, string nSensores, string metodo) {
	Ligar();

	stmt = con->createStatement();


	stmt->execute("INSERT INTO configuracao(NomeConfiguracao,NumeroSensores,Metodo,TempoViragem) Values('" + nomeConfig + "','" + nSensores + "','" + metodo + "','" + tempoV + "');");

	delete stmt;
	delete con;

}

void BaseDados::inserirEquipaBD(string nomeEquipa, string nEle1, string nEle2, string nEle3, string nEle4, string nEle5, string nEle6, string nEle7, string nEle8) {

	Ligar();

	stmt = con->createStatement();


	stmt->execute("INSERT INTO equipa(NomeEquipa,NomeEle1,NomeEle2,NomeEle3,NomeEle4,NomeEle5,NomeEle6,NomeEle7,NomeEle8) Values('" + nomeEquipa + "','" + nEle1 + "','" + nEle2 + "','" + nEle3 + "','" + nEle4 + "','" + nEle5 + "','" + nEle6 + "','" + nEle7 + "','" + nEle8 + "');");

	delete stmt;
	delete con;
}

void BaseDados::inserirRobotBD(string nomeRobot, string nomeEquipa, string nomeConfig) {
	int id, idC;


	id = getIDEquipa(nomeEquipa);
	string idEquipa = to_string(id);

	idC = getIDConfig(nomeConfig);
	string idConfig = to_string(idC);

	Ligar();

	stmt = con->createStatement();

	stmt->execute("INSERT INTO robot(NomeRobot,IDEquipa, IDConfiguracao) Values('" + nomeRobot + "','" + idEquipa + "','" + idConfig + "');");

	delete stmt;
	delete con;
}

void BaseDados::eliminarProvaBD(string nomeProva, string localProva) {
	int id;


	id = getIDProva(nomeProva, localProva);
	string idEscolhido = to_string(id);



	Ligar();

	stmt = con->createStatement();

	stmt->execute("Delete FROM Prova where IDProva=" + idEscolhido + ";");

	delete stmt;
	delete con;

}

void BaseDados::eliminarRobotBD(string nomeRobot, string nomeEquipa, string nomeConfig) {

	int id;

	id = getIDRobot(nomeRobot, nomeEquipa, nomeConfig);

	string idEscolhido = to_string(id);

	Ligar();

	stmt = con->createStatement();

	stmt->execute("Delete FROM Robot where IDRobot=" + idEscolhido + ";");

	delete stmt;
	delete con;

}

void BaseDados::eliminarConfigBD(string nomeConfig, string nSensores, string tempoV, string metodo) {
	int id;

	id = getIDConfig(nomeConfig);

	string idEscolhido = to_string(id);

	Ligar();

	stmt = con->createStatement();

	stmt->execute("Delete FROM Configuracao where IDConfiguracao=" + idEscolhido + ";");

	delete stmt;
	delete con;
}

void BaseDados::eliminarEquipaBD(string nomeEquipa) {
	int id;



	id = getIDEquipa(nomeEquipa);

	string idEscolhido = to_string(id);

	Ligar();

	stmt = con->createStatement();


	stmt->execute("Delete FROM Equipa where IDEquipa=" + idEscolhido + ";");

	delete stmt;
	delete con;

}

void BaseDados::modificarProvaBD(int id, string nomeProva, string localProva, string data) {

	Ligar();

	stmt = con->createStatement();

	string idEscolhido = to_string(id);

	stmt->executeUpdate("UPDATE prova SET NomeProva = '" + nomeProva + "', LocalProva = '" + localProva + "', DataProva = '" + data + "' WHERE IDProva = '" + idEscolhido + "';");

	delete stmt;
	delete con;

}

void BaseDados::modificarRobotBD(int id, string nomeRobot, string nomeEquipa, string nomeConfig) {


	int idEquipa = getIDEquipa(nomeEquipa);
	string idEscolhido = to_string(id);

	int idconfig = getIDConfig(nomeConfig);
	string idConfig = to_string(idconfig);

	Ligar();

	stmt = con->createStatement();

	stmt->executeUpdate("UPDATE robot SET NomeRobot = '" + nomeRobot + "', IDEquipa = '" + to_string(idEquipa) + "', IDConfiguracao ='" + idConfig + "' WHERE IDRobot = '" + idEscolhido + "';");

	delete stmt;
	delete con;

}

void BaseDados::modificarEquipaBD(int id, string nomeEquipa, string nEle1, string nEle2, string nEle3, string nEle4, string nEle5, string nEle6, string nEle7, string nEle8) {

	Ligar();

	stmt = con->createStatement();

	string idEscolhido = to_string(id);

	stmt->executeUpdate("UPDATE equipa SET NomeEquipa = '" + nomeEquipa + "', NomeEle1 = '" + nEle1 + "', NomeEle2 = '" + nEle2 + "', NomeEle3 = '" + nEle3 + "', NomeEle4 = '" + nEle4 + "', NomeEle5 = '" + nEle5 + "', NomeEle6 = '" + nEle6 + "', NomeEle7 = '" + nEle7 + "', NomeEle8 = '" + nEle8 + "' WHERE IDEquipa = '" + idEscolhido + "';");

	delete stmt;
	delete con;
}

void BaseDados::modificarConfigBD(int id, string nomeConfig, string tempoV, string nSensores, string metodo) {

	Ligar();

	stmt = con->createStatement();

	string idEscolhido = to_string(id);

	stmt->executeUpdate("UPDATE configuracao SET NomeConfiguracao = '" + nomeConfig + "', NumeroSensores = '" + nSensores + "', Metodo = '" + metodo + "', TempoViragem = '" + tempoV + "' WHERE IDConfiguracao = '" + idEscolhido + "';");

	delete stmt;
	delete con;
}

void BaseDados::inserirProvaLista(vector<string>* nomes, vector<string>* locais, vector<string> * datas) {
	string nomeP, localP, dataP;

	Ligar();
	
	stmt = con->createStatement();

	res = stmt->executeQuery("SELECT * FROM prova ");

	while (res->next()) {

		nomeP = res->getString("NomeProva");
		nomes->push_back(nomeP);

		localP = res->getString("LocalProva");
		locais->push_back(localP);

		dataP = res->getString("DataProva");
		datas->push_back(dataP);
	}

	locais;
	nomes;
	datas;

	delete res;
	delete stmt;
	delete con;
}

void BaseDados::inserirRobotLista(vector<string> *nomesR, vector<string> *nomesE, vector<string> *nomesC) {
	Ligar();

	stmt = con->createStatement();

	res = stmt->executeQuery("SELECT * FROM robot ");

	while (res->next()) {

		string nomeR = res->getString("NomeRobot");
		nomesR->push_back(nomeR);

		int id = res->getInt("IDEquipa");
		string nomeE = getNomeEquipa(id);
		nomesE->push_back(nomeE);

		int idc = res->getInt("IDConfiguracao");
		string nomeC = getNomeConfig(idc);
		nomesC->push_back(nomeC);
	}

	nomesR;
	nomesE;
	nomesC;

	delete stmt;
	delete res;
	delete con;
}

void BaseDados::inserirEquipaLista(vector<string>* nomesE, vector<string>* nomesC, vector<string>* nomes2, vector<string>* nomes3, vector<string>* nomes4, vector<string>* nomes5, vector<string>* nomes6, vector<string>* nomes7, vector<string>* nomes8) {

	Ligar();

	stmt = con->createStatement();

	res = stmt->executeQuery("SELECT * FROM equipa ");

	while (res->next()) {

		string nomeE = res->getString("NomeEquipa");
		nomesE->push_back(nomeE);

		string nomeC = res->getString("NomeEle1");
		nomesC->push_back(nomeC);

		string nome2 = res->getString("NomeEle2");
		nomes2->push_back(nome2);

		string nome3 = res->getString("NomeEle3");
		nomes3->push_back(nome3);

		string nome4 = res->getString("NomeEle4");
		nomes4->push_back(nome4);

		string nome5 = res->getString("NomeEle5");
		nomes5->push_back(nome5);

		string nome6 = res->getString("NomeEle6");
		nomes6->push_back(nome6);

		string nome7 = res->getString("NomeEle7");
		nomes7->push_back(nome7);

		string nome8 = res->getString("NomeEle8");
		nomes8->push_back(nome8);

	}


	nomesE, nomesC, nomes2, nomes3, nomes4, nomes5, nomes6, nomes7, nomes8;

	delete stmt;
	delete res;
	delete con;
}

void BaseDados::inserirConfigLista(vector<string>* nomesC, vector<string>* nsensores, vector<string> * temposV, vector<string> * metodos) {

	Ligar();

	stmt = con->createStatement();

	res = stmt->executeQuery("SELECT * FROM configuracao ");

	while (res->next()) {

		string nomeC = res->getString("NomeConfiguracao");
		nomesC->push_back(nomeC);

		string sensor = res->getString("NumeroSensores");
		nsensores->push_back(sensor);

		string metodo = res->getString("Metodo");
		metodos->push_back(metodo);

		string tempo = res->getString("TempoViragem");
		temposV->push_back(tempo);

	}

	nomesC, nsensores, temposV, metodos;
	
	delete stmt;
	delete res;
	delete con;

}