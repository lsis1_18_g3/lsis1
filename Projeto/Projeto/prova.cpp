#include "prova.h"

Prova::Prova(QObject *parent) : QObject(parent)
{

}

QString Prova::nomeProva() const
{
	return m_nomeProva;
}

void Prova::setNomeProva(const QString &nomeProva) {

	m_nomeProva = nomeProva;
}

QString Prova::LocalProva() const
{
	return m_LocalProva;
}

void Prova::setLocalProva(const QString &LocalProva)
{
	m_LocalProva = LocalProva;

}

QDate Prova::DataProva() const
{
	return m_DataProva;
}

void Prova::setDataProva(const QDate &DataProva)
{
	m_DataProva = DataProva;
}