#ifndef EQUIPA_H
#define EQUIPA_H

#include <QObject>
#include <QString>
#include <QStringList>

class Equipa : public QObject
{
	Q_OBJECT

public:
	explicit Equipa(QObject *parent = nullptr);


	QString nomeEquipa() const;
	void setNomeEquipa(const QString &nomeEquipa);


	QString nomeEle1() const;
	void setNomeEle1(const QString &nomeEle1);

	QString nomeEle2() const;
	void setNomeEle2(const QString &nomeEle2);

	QString nomeEle3() const;
	void setNomeEle3(const QString &nomeEle3);

	QString nomeEle4() const;
	void setNomeEle4(const QString &nomeEle4);

	QString nomeEle5() const;
	void setNomeEle5(const QString &nomeEle5);

	QString nomeEle6() const;
	void setNomeEle6(const QString &nomeEle6);

	QString nomeEle7() const;
	void setNomeEle7(const QString &nomeEle7);

	QString nomeEle8() const;
	void setNomeEle8(const QString &nomeEle8);

signals:


public slots:


private:
	QString m_nomeEquipa;
	QString m_nomeEle1;
	QString m_nomeEle2;
	QString m_nomeEle3;
	QString m_nomeEle4;
	QString m_nomeEle5;
	QString m_nomeEle6;
	QString m_nomeEle7;
	QString m_nomeEle8;

};

#endif // EQUIPA_H

