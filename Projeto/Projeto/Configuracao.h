#ifndef CONFIGURACAO_H
#define CONFIGURACAO_H

#include <QObject>
#include <QString>
#include <QStringList>

class Configuracao : public QObject
{
	Q_OBJECT

public:
	explicit Configuracao(QObject *parent = nullptr);
	
	QString metodo() const;
	void setMetodo(const QString &metodo);

	QString nSesores() const;
	void setNSensores(const QString &nSensores);

	QString tempoViragem() const;
	void setTempoViragem(const QString &tempoViragem);

	QString nomeConfig() const;
	void setNomeConfig(const QString &nomeConfig);

signals:

public slots:

private:

	QString m_metodo;
	QString m_nSensores;
	QString m_tempoViragem;
	QString m_nomeConfig;


};

#endif