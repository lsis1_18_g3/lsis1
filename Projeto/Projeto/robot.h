#ifndef ROBOT_H
#define ROBOT_H

#include <equipa.h>

#include <QObject>
#include <QString>
#include <QStringList>

class Robot : public QObject
{
	Q_OBJECT

public:
	explicit Robot(QObject *parent = nullptr);

	QString nomeRobot() const;
	void setNomeRobot(const QString &nomeRobot);

	Equipa equipa() const;
	void setEquipa(const Equipa &equipa);

	QString nomeEquipa() const;
	void setNomeEquipa(const QString &nomeEquipa);

	QString nomeConfig() const;
	void setNomeConfig(const QString &nomeConfig);

signals:


public slots:

private:
	QString m_nomeRobot;
	QString m_nomeEquipa;
	QString m_nomeConfig;
};

#endif // ROBOT_H

