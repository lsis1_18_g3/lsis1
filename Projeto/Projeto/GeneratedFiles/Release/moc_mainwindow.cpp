/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[98];
    char stringdata0[1782];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 12), // "inserirProva"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 18), // "inserirProvaTabela"
QT_MOC_LITERAL(4, 44, 6), // "Prova*"
QT_MOC_LITERAL(5, 51, 5), // "prova"
QT_MOC_LITERAL(6, 57, 21), // "eliminarRowTableProva"
QT_MOC_LITERAL(7, 79, 23), // "actualizarRowTableProva"
QT_MOC_LITERAL(8, 103, 10), // "nomeAntigo"
QT_MOC_LITERAL(9, 114, 18), // "inserirProvafromBD"
QT_MOC_LITERAL(10, 133, 14), // "modificarProva"
QT_MOC_LITERAL(11, 148, 13), // "eliminarProva"
QT_MOC_LITERAL(12, 162, 4), // "save"
QT_MOC_LITERAL(13, 167, 7), // "discard"
QT_MOC_LITERAL(14, 175, 5), // "reset"
QT_MOC_LITERAL(15, 181, 30), // "on_actionListarProva_triggered"
QT_MOC_LITERAL(16, 212, 27), // "on_actionInsProva_triggered"
QT_MOC_LITERAL(17, 240, 27), // "on_actionModProva_triggered"
QT_MOC_LITERAL(18, 268, 27), // "on_actionEliProva_triggered"
QT_MOC_LITERAL(19, 296, 21), // "on_buttonBox_accepted"
QT_MOC_LITERAL(20, 318, 21), // "on_buttonBox_rejected"
QT_MOC_LITERAL(21, 340, 20), // "on_buttonBox_clicked"
QT_MOC_LITERAL(22, 361, 16), // "QAbstractButton*"
QT_MOC_LITERAL(23, 378, 6), // "button"
QT_MOC_LITERAL(24, 385, 10), // "clearProva"
QT_MOC_LITERAL(25, 396, 27), // "on_actionDetProva_triggered"
QT_MOC_LITERAL(26, 424, 12), // "inserirRobot"
QT_MOC_LITERAL(27, 437, 18), // "inserirRobotTabela"
QT_MOC_LITERAL(28, 456, 6), // "Robot*"
QT_MOC_LITERAL(29, 463, 5), // "robot"
QT_MOC_LITERAL(30, 469, 21), // "eliminarRowTableRobot"
QT_MOC_LITERAL(31, 491, 22), // "actulizarRowTableRobot"
QT_MOC_LITERAL(32, 514, 10), // "nomeantigo"
QT_MOC_LITERAL(33, 525, 18), // "inserirRobotfromBD"
QT_MOC_LITERAL(34, 544, 14), // "modificarRobot"
QT_MOC_LITERAL(35, 559, 13), // "eliminarRobot"
QT_MOC_LITERAL(36, 573, 5), // "saveR"
QT_MOC_LITERAL(37, 579, 8), // "discardR"
QT_MOC_LITERAL(38, 588, 6), // "resetR"
QT_MOC_LITERAL(39, 595, 30), // "on_actionListarRobot_triggered"
QT_MOC_LITERAL(40, 626, 27), // "on_actionInsRobot_triggered"
QT_MOC_LITERAL(41, 654, 27), // "on_actionModRobot_triggered"
QT_MOC_LITERAL(42, 682, 27), // "on_actionEliRobot_triggered"
QT_MOC_LITERAL(43, 710, 26), // "on_buttonBoxRobot_accepted"
QT_MOC_LITERAL(44, 737, 26), // "on_buttonBoxRobot_rejected"
QT_MOC_LITERAL(45, 764, 25), // "on_buttonBoxRobot_clicked"
QT_MOC_LITERAL(46, 790, 10), // "clearRobot"
QT_MOC_LITERAL(47, 801, 27), // "on_actionDetRobot_triggered"
QT_MOC_LITERAL(48, 829, 13), // "inserirEquipa"
QT_MOC_LITERAL(49, 843, 19), // "inserirEquipaTabela"
QT_MOC_LITERAL(50, 863, 7), // "Equipa*"
QT_MOC_LITERAL(51, 871, 6), // "equipa"
QT_MOC_LITERAL(52, 878, 24), // "actualizarRowTableEquipa"
QT_MOC_LITERAL(53, 903, 22), // "eliminarRowTableEquipa"
QT_MOC_LITERAL(54, 926, 19), // "inserirEquipafromBD"
QT_MOC_LITERAL(55, 946, 15), // "modificarEquipa"
QT_MOC_LITERAL(56, 962, 14), // "eliminarEquipa"
QT_MOC_LITERAL(57, 977, 5), // "saveE"
QT_MOC_LITERAL(58, 983, 8), // "discardE"
QT_MOC_LITERAL(59, 992, 6), // "resetE"
QT_MOC_LITERAL(60, 999, 31), // "on_actionListarEquipa_triggered"
QT_MOC_LITERAL(61, 1031, 28), // "on_actionInsEquipa_triggered"
QT_MOC_LITERAL(62, 1060, 28), // "on_actionModEquipa_triggered"
QT_MOC_LITERAL(63, 1089, 28), // "on_actionEliEquipa_triggered"
QT_MOC_LITERAL(64, 1118, 27), // "on_buttonBoxEquipa_accepted"
QT_MOC_LITERAL(65, 1146, 27), // "on_buttonBoxEquipa_rejected"
QT_MOC_LITERAL(66, 1174, 26), // "on_buttonBoxEquipa_clicked"
QT_MOC_LITERAL(67, 1201, 11), // "clearEquipa"
QT_MOC_LITERAL(68, 1213, 28), // "on_actionDetEquipa_triggered"
QT_MOC_LITERAL(69, 1242, 13), // "inserirConfig"
QT_MOC_LITERAL(70, 1256, 19), // "inserirConfigTabela"
QT_MOC_LITERAL(71, 1276, 13), // "Configuracao*"
QT_MOC_LITERAL(72, 1290, 6), // "config"
QT_MOC_LITERAL(73, 1297, 23), // "actulizarRowTableConfig"
QT_MOC_LITERAL(74, 1321, 22), // "eliminarRowTableConfig"
QT_MOC_LITERAL(75, 1344, 19), // "inserirConfigfromBD"
QT_MOC_LITERAL(76, 1364, 21), // "modificarConfiguracao"
QT_MOC_LITERAL(77, 1386, 20), // "eliminarConfiguracao"
QT_MOC_LITERAL(78, 1407, 5), // "saveC"
QT_MOC_LITERAL(79, 1413, 8), // "discardC"
QT_MOC_LITERAL(80, 1422, 6), // "resetC"
QT_MOC_LITERAL(81, 1429, 11), // "clearConfig"
QT_MOC_LITERAL(82, 1441, 27), // "on_actionLisConfi_triggered"
QT_MOC_LITERAL(83, 1469, 28), // "on_actionDetConfig_triggered"
QT_MOC_LITERAL(84, 1498, 28), // "on_actionInsConfig_triggered"
QT_MOC_LITERAL(85, 1527, 28), // "on_actionModConfig_triggered"
QT_MOC_LITERAL(86, 1556, 28), // "on_actionEliConfig_triggered"
QT_MOC_LITERAL(87, 1585, 27), // "on_buttonBoxConfig_accepted"
QT_MOC_LITERAL(88, 1613, 27), // "on_buttonBoxConfig_rejected"
QT_MOC_LITERAL(89, 1641, 26), // "on_buttonBoxConfig_clicked"
QT_MOC_LITERAL(90, 1668, 9), // "ModoProva"
QT_MOC_LITERAL(91, 1678, 4), // "fase"
QT_MOC_LITERAL(92, 1683, 32), // "on_actionIniciar_Prova_triggered"
QT_MOC_LITERAL(93, 1716, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(94, 1738, 5), // "Fases"
QT_MOC_LITERAL(95, 1744, 12), // "receberFases"
QT_MOC_LITERAL(96, 1757, 10), // "readSerial"
QT_MOC_LITERAL(97, 1768, 13) // "updateEstados"

    },
    "MainWindow\0inserirProva\0\0inserirProvaTabela\0"
    "Prova*\0prova\0eliminarRowTableProva\0"
    "actualizarRowTableProva\0nomeAntigo\0"
    "inserirProvafromBD\0modificarProva\0"
    "eliminarProva\0save\0discard\0reset\0"
    "on_actionListarProva_triggered\0"
    "on_actionInsProva_triggered\0"
    "on_actionModProva_triggered\0"
    "on_actionEliProva_triggered\0"
    "on_buttonBox_accepted\0on_buttonBox_rejected\0"
    "on_buttonBox_clicked\0QAbstractButton*\0"
    "button\0clearProva\0on_actionDetProva_triggered\0"
    "inserirRobot\0inserirRobotTabela\0Robot*\0"
    "robot\0eliminarRowTableRobot\0"
    "actulizarRowTableRobot\0nomeantigo\0"
    "inserirRobotfromBD\0modificarRobot\0"
    "eliminarRobot\0saveR\0discardR\0resetR\0"
    "on_actionListarRobot_triggered\0"
    "on_actionInsRobot_triggered\0"
    "on_actionModRobot_triggered\0"
    "on_actionEliRobot_triggered\0"
    "on_buttonBoxRobot_accepted\0"
    "on_buttonBoxRobot_rejected\0"
    "on_buttonBoxRobot_clicked\0clearRobot\0"
    "on_actionDetRobot_triggered\0inserirEquipa\0"
    "inserirEquipaTabela\0Equipa*\0equipa\0"
    "actualizarRowTableEquipa\0"
    "eliminarRowTableEquipa\0inserirEquipafromBD\0"
    "modificarEquipa\0eliminarEquipa\0saveE\0"
    "discardE\0resetE\0on_actionListarEquipa_triggered\0"
    "on_actionInsEquipa_triggered\0"
    "on_actionModEquipa_triggered\0"
    "on_actionEliEquipa_triggered\0"
    "on_buttonBoxEquipa_accepted\0"
    "on_buttonBoxEquipa_rejected\0"
    "on_buttonBoxEquipa_clicked\0clearEquipa\0"
    "on_actionDetEquipa_triggered\0inserirConfig\0"
    "inserirConfigTabela\0Configuracao*\0"
    "config\0actulizarRowTableConfig\0"
    "eliminarRowTableConfig\0inserirConfigfromBD\0"
    "modificarConfiguracao\0eliminarConfiguracao\0"
    "saveC\0discardC\0resetC\0clearConfig\0"
    "on_actionLisConfi_triggered\0"
    "on_actionDetConfig_triggered\0"
    "on_actionInsConfig_triggered\0"
    "on_actionModConfig_triggered\0"
    "on_actionEliConfig_triggered\0"
    "on_buttonBoxConfig_accepted\0"
    "on_buttonBoxConfig_rejected\0"
    "on_buttonBoxConfig_clicked\0ModoProva\0"
    "fase\0on_actionIniciar_Prova_triggered\0"
    "on_pushButton_clicked\0Fases\0receberFases\0"
    "readSerial\0updateEstados"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      83,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  429,    2, 0x08 /* Private */,
       3,    1,  430,    2, 0x08 /* Private */,
       6,    1,  433,    2, 0x08 /* Private */,
       7,    2,  436,    2, 0x08 /* Private */,
       9,    0,  441,    2, 0x08 /* Private */,
      10,    0,  442,    2, 0x08 /* Private */,
      11,    0,  443,    2, 0x08 /* Private */,
      12,    0,  444,    2, 0x08 /* Private */,
      13,    0,  445,    2, 0x08 /* Private */,
      14,    0,  446,    2, 0x08 /* Private */,
      15,    0,  447,    2, 0x08 /* Private */,
      16,    0,  448,    2, 0x08 /* Private */,
      17,    0,  449,    2, 0x08 /* Private */,
      18,    0,  450,    2, 0x08 /* Private */,
      19,    0,  451,    2, 0x08 /* Private */,
      20,    0,  452,    2, 0x08 /* Private */,
      21,    1,  453,    2, 0x08 /* Private */,
      24,    0,  456,    2, 0x08 /* Private */,
      25,    0,  457,    2, 0x08 /* Private */,
      26,    0,  458,    2, 0x08 /* Private */,
      27,    1,  459,    2, 0x08 /* Private */,
      30,    1,  462,    2, 0x08 /* Private */,
      31,    2,  465,    2, 0x08 /* Private */,
      33,    0,  470,    2, 0x08 /* Private */,
      34,    0,  471,    2, 0x08 /* Private */,
      35,    0,  472,    2, 0x08 /* Private */,
      36,    0,  473,    2, 0x08 /* Private */,
      37,    0,  474,    2, 0x08 /* Private */,
      38,    0,  475,    2, 0x08 /* Private */,
      39,    0,  476,    2, 0x08 /* Private */,
      40,    0,  477,    2, 0x08 /* Private */,
      41,    0,  478,    2, 0x08 /* Private */,
      42,    0,  479,    2, 0x08 /* Private */,
      43,    0,  480,    2, 0x08 /* Private */,
      44,    0,  481,    2, 0x08 /* Private */,
      45,    1,  482,    2, 0x08 /* Private */,
      46,    0,  485,    2, 0x08 /* Private */,
      47,    0,  486,    2, 0x08 /* Private */,
      48,    0,  487,    2, 0x08 /* Private */,
      49,    1,  488,    2, 0x08 /* Private */,
      52,    2,  491,    2, 0x08 /* Private */,
      53,    1,  496,    2, 0x08 /* Private */,
      54,    0,  499,    2, 0x08 /* Private */,
      55,    0,  500,    2, 0x08 /* Private */,
      56,    0,  501,    2, 0x08 /* Private */,
      57,    0,  502,    2, 0x08 /* Private */,
      58,    0,  503,    2, 0x08 /* Private */,
      59,    0,  504,    2, 0x08 /* Private */,
      60,    0,  505,    2, 0x08 /* Private */,
      61,    0,  506,    2, 0x08 /* Private */,
      62,    0,  507,    2, 0x08 /* Private */,
      63,    0,  508,    2, 0x08 /* Private */,
      64,    0,  509,    2, 0x08 /* Private */,
      65,    0,  510,    2, 0x08 /* Private */,
      66,    1,  511,    2, 0x08 /* Private */,
      67,    0,  514,    2, 0x08 /* Private */,
      68,    0,  515,    2, 0x08 /* Private */,
      69,    0,  516,    2, 0x08 /* Private */,
      70,    1,  517,    2, 0x08 /* Private */,
      73,    2,  520,    2, 0x08 /* Private */,
      74,    1,  525,    2, 0x08 /* Private */,
      75,    0,  528,    2, 0x08 /* Private */,
      76,    0,  529,    2, 0x08 /* Private */,
      77,    0,  530,    2, 0x08 /* Private */,
      78,    0,  531,    2, 0x08 /* Private */,
      79,    0,  532,    2, 0x08 /* Private */,
      80,    0,  533,    2, 0x08 /* Private */,
      81,    0,  534,    2, 0x08 /* Private */,
      82,    0,  535,    2, 0x08 /* Private */,
      83,    0,  536,    2, 0x08 /* Private */,
      84,    0,  537,    2, 0x08 /* Private */,
      85,    0,  538,    2, 0x08 /* Private */,
      86,    0,  539,    2, 0x08 /* Private */,
      87,    0,  540,    2, 0x08 /* Private */,
      88,    0,  541,    2, 0x08 /* Private */,
      89,    1,  542,    2, 0x08 /* Private */,
      90,    1,  545,    2, 0x08 /* Private */,
      92,    0,  548,    2, 0x08 /* Private */,
      93,    0,  549,    2, 0x08 /* Private */,
      94,    0,  550,    2, 0x08 /* Private */,
      95,    0,  551,    2, 0x08 /* Private */,
      96,    0,  552,    2, 0x08 /* Private */,
      97,    1,  553,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 4,    8,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 22,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 28,   32,   29,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 22,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 50,   51,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 50,   32,   51,
    QMetaType::Void, 0x80000000 | 50,   51,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 22,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 71,   72,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 71,   32,   72,
    QMetaType::Void, 0x80000000 | 71,   72,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 22,   23,
    QMetaType::Void, QMetaType::Int,   91,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->inserirProva(); break;
        case 1: _t->inserirProvaTabela((*reinterpret_cast< Prova*(*)>(_a[1]))); break;
        case 2: _t->eliminarRowTableProva((*reinterpret_cast< Prova*(*)>(_a[1]))); break;
        case 3: _t->actualizarRowTableProva((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< Prova*(*)>(_a[2]))); break;
        case 4: _t->inserirProvafromBD(); break;
        case 5: _t->modificarProva(); break;
        case 6: _t->eliminarProva(); break;
        case 7: _t->save(); break;
        case 8: _t->discard(); break;
        case 9: _t->reset(); break;
        case 10: _t->on_actionListarProva_triggered(); break;
        case 11: _t->on_actionInsProva_triggered(); break;
        case 12: _t->on_actionModProva_triggered(); break;
        case 13: _t->on_actionEliProva_triggered(); break;
        case 14: _t->on_buttonBox_accepted(); break;
        case 15: _t->on_buttonBox_rejected(); break;
        case 16: _t->on_buttonBox_clicked((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 17: _t->clearProva(); break;
        case 18: _t->on_actionDetProva_triggered(); break;
        case 19: _t->inserirRobot(); break;
        case 20: _t->inserirRobotTabela((*reinterpret_cast< Robot*(*)>(_a[1]))); break;
        case 21: _t->eliminarRowTableRobot((*reinterpret_cast< Robot*(*)>(_a[1]))); break;
        case 22: _t->actulizarRowTableRobot((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< Robot*(*)>(_a[2]))); break;
        case 23: _t->inserirRobotfromBD(); break;
        case 24: _t->modificarRobot(); break;
        case 25: _t->eliminarRobot(); break;
        case 26: _t->saveR(); break;
        case 27: _t->discardR(); break;
        case 28: _t->resetR(); break;
        case 29: _t->on_actionListarRobot_triggered(); break;
        case 30: _t->on_actionInsRobot_triggered(); break;
        case 31: _t->on_actionModRobot_triggered(); break;
        case 32: _t->on_actionEliRobot_triggered(); break;
        case 33: _t->on_buttonBoxRobot_accepted(); break;
        case 34: _t->on_buttonBoxRobot_rejected(); break;
        case 35: _t->on_buttonBoxRobot_clicked((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 36: _t->clearRobot(); break;
        case 37: _t->on_actionDetRobot_triggered(); break;
        case 38: _t->inserirEquipa(); break;
        case 39: _t->inserirEquipaTabela((*reinterpret_cast< Equipa*(*)>(_a[1]))); break;
        case 40: _t->actualizarRowTableEquipa((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< Equipa*(*)>(_a[2]))); break;
        case 41: _t->eliminarRowTableEquipa((*reinterpret_cast< Equipa*(*)>(_a[1]))); break;
        case 42: _t->inserirEquipafromBD(); break;
        case 43: _t->modificarEquipa(); break;
        case 44: _t->eliminarEquipa(); break;
        case 45: _t->saveE(); break;
        case 46: _t->discardE(); break;
        case 47: _t->resetE(); break;
        case 48: _t->on_actionListarEquipa_triggered(); break;
        case 49: _t->on_actionInsEquipa_triggered(); break;
        case 50: _t->on_actionModEquipa_triggered(); break;
        case 51: _t->on_actionEliEquipa_triggered(); break;
        case 52: _t->on_buttonBoxEquipa_accepted(); break;
        case 53: _t->on_buttonBoxEquipa_rejected(); break;
        case 54: _t->on_buttonBoxEquipa_clicked((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 55: _t->clearEquipa(); break;
        case 56: _t->on_actionDetEquipa_triggered(); break;
        case 57: _t->inserirConfig(); break;
        case 58: _t->inserirConfigTabela((*reinterpret_cast< Configuracao*(*)>(_a[1]))); break;
        case 59: _t->actulizarRowTableConfig((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< Configuracao*(*)>(_a[2]))); break;
        case 60: _t->eliminarRowTableConfig((*reinterpret_cast< Configuracao*(*)>(_a[1]))); break;
        case 61: _t->inserirConfigfromBD(); break;
        case 62: _t->modificarConfiguracao(); break;
        case 63: _t->eliminarConfiguracao(); break;
        case 64: _t->saveC(); break;
        case 65: _t->discardC(); break;
        case 66: _t->resetC(); break;
        case 67: _t->clearConfig(); break;
        case 68: _t->on_actionLisConfi_triggered(); break;
        case 69: _t->on_actionDetConfig_triggered(); break;
        case 70: _t->on_actionInsConfig_triggered(); break;
        case 71: _t->on_actionModConfig_triggered(); break;
        case 72: _t->on_actionEliConfig_triggered(); break;
        case 73: _t->on_buttonBoxConfig_accepted(); break;
        case 74: _t->on_buttonBoxConfig_rejected(); break;
        case 75: _t->on_buttonBoxConfig_clicked((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 76: _t->ModoProva((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 77: _t->on_actionIniciar_Prova_triggered(); break;
        case 78: _t->on_pushButton_clicked(); break;
        case 79: _t->Fases(); break;
        case 80: _t->receberFases(); break;
        case 81: _t->readSerial(); break;
        case 82: _t->updateEstados((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Prova* >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Prova* >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Prova* >(); break;
            }
            break;
        case 16:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractButton* >(); break;
            }
            break;
        case 20:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Robot* >(); break;
            }
            break;
        case 21:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Robot* >(); break;
            }
            break;
        case 22:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Robot* >(); break;
            }
            break;
        case 35:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractButton* >(); break;
            }
            break;
        case 39:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Equipa* >(); break;
            }
            break;
        case 40:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Equipa* >(); break;
            }
            break;
        case 41:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Equipa* >(); break;
            }
            break;
        case 54:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractButton* >(); break;
            }
            break;
        case 58:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Configuracao* >(); break;
            }
            break;
        case 59:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Configuracao* >(); break;
            }
            break;
        case 60:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Configuracao* >(); break;
            }
            break;
        case 75:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractButton* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 83)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 83;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 83)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 83;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
